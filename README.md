# Technical Test 

This project aims to spin up three servers with nginx and one of them acts as load balancer for the other two.

## 1) Install dependencies (VirtualBox, Vagrant, Ansible)

  1. Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads).
  2. Download and install [Vagrant](http://www.vagrantup.com/downloads.html).
  3. [Mac/Linux only] Install [Ansible](http://docs.ansible.com/intro_installation.html).

## 2) Getting started
  1. Download this project and put it wherever you want.

  **Files/Folders in the Project:**
  
    `application` : A sample NodeJS application which runs on port 3000
    `playbooks` : Ansible playbooks with roles,vars,tests and main playbooks ( provision.yml and deploy.yml)
    `inventory` : Ansible inventory file 
    `Vagrantfile` : Vagrant file to provision VMs
  
  2. Open Terminal, cd to this directory (containing the `Vagrantfile` and this README file).

  3. Run below commands  
  
```
$cd <directory_where_the_files_are_downloaded>

$vagrant up

$ansible-playbook playbooks/deploy.yml -i inventory
```

Finally, once all your tests are done ,run the following command to destroy VMs.
```
$vagrant destroy -f
```

## 3) Implementation Details

#### $vagrant up 
The above command provisions theree 'ubuntu/bionic64' VMs, one for load-balancer and other two for running the application.

These VMs are identified as 

`loadbalancer (192.168.4.2)`

`app1server (192.168.4.3)` and 

`app2server (192.168.4.4)`

It also do basic configuration of opening ports on the servers and update apt cache on the system.

#### $ansible-playbook playbooks/deploy.yml -i inventory
The above command configures the servers as mentioned below and deplpoy application on two application servers. 
Once the application is deployed automation tests run to make sure the system is wokring as expected.

  * `app1server` : nginx and nodejs are installed ; nginx is used as proxy for nginx server
  * `app2server` : nginx and nodejs are installed ; nginx is used as proxy for nginx server
  * `loadbalancer` : nginx is installed ; configured nginx to act as loadbalancer for app1server and app2server

## 4) Application Details

It is a simple Node.Js Application that servers data on port 3000.( Please refer to  application folder for details).

The web application displays the name of the server being served from and a random quote from database configured in AWS.

## 5) Accesssing Servers 
  Open Browser and enter url

  * Load Balancer ( Refresh page to see content served from two servers)
  ```
    http://192.168.4.2/
  ```
  * Accessing application servers  over Node Js port (3000);This port can be disabled later as nginx proxy is enabled
  ```
    http://192.168.4.3:3000/
    http://192.168.4.4:3000/
  ```
  * Accessing application servers over 80 port (nginx proxy for node servers)
  ```
    http://192.168.4.3/
    http://192.168.4.4/
  ```

## 6)Automation Tests
Automation tests run as a part of ```$ansible-playbook playbooks/deploy.yml -i inventory``` .
It has got four tests ( placed under playbooks/tests/tasks/main.yml)
  * Access application server1  url - verify content is served from `app1server`
  * Access application server2  url - verify content is served from `app2server`
  * Access load balancer url - verify it is served from `app1server` for the first time 
  * Access load balancer url again - verify it is served from `app2server` 

## 7) Improvements / Alternates

  * `applcation` can be containerised using docker
  * `application` also can be placed in git repository and ansible can be cconfigured to deploy it from git instead of copying to servers. The reason I kept it in the folder is because of easy of demonstration ( all in one folder).FYI, Code is commented out in `playbooks/roles/nodejs/tasks/main.yml`
  * Terraform can be used to provision resources in cloud replacing Vagrant  
  * Full CI/CD pipeline can be configured either in Jenkis or git actions.
  * Ansible galaxy roles/collections can be used instead of writing from scratch.
  
**Bloppers:**
  * I tried with another VM to hold database but my system started freezing, so I provisioned database in AWS RDS. I commented out this in `Vagrantfile` and  `playbooks/deploy.yml` files

## Author 
Sudeer Vendra
