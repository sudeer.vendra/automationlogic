var os = require('os')
var http = require('http')
var mysql      = require('mysql');

var quote_of_the_day_author;
var quote_of_the_day;
var random_num = Math.floor(Math.random() * 10) + 1 ;

// Database connection 
// It is not a good practise to place credentials in the code , this is just for demonstation
// In real applicaiton, we place these in AWS Secret Manager or AWS System Manager Parameter Store
// We also encrypt them using KMS
var connection = mysql.createConnection({
  host     : 'database-1.ctfpioegxxcj.us-east-1.rds.amazonaws.com',
  user     : 'admin',
  password : 'sudeer12345',
  database : 'my_test_db'
});

// Retrive quotes from database and hold them in variables
connection.connect(function(error){
	if(!!error) {
		console.log(error);
	} else {
    console.log('Connected..!');
    connection.query('SELECT * from my_test_db.quotes where id='+random_num+';', function (error, results, fields) {
      
      quote_of_the_day = results[0].quote
      quote_of_the_day_author = results[0].author
      console.log('random_num='+random_num)
      console.log('quote_of_the_day');
      console.log('quote_of_the_day_author');
          
    });
	}
});

// Display content to the user
function handleRequest(req, res) {

  res.write('App is Up and Running!\n');
  res.write('Hello Sudeer I\'m being served from ' + os.hostname());

  res.write('\n\nQuote of the day!:\n');
  res.write(quote_of_the_day);

  res.write('\n\nAuthor:\n');
  res.write(quote_of_the_day_author);

  res.end();
}

http.createServer(handleRequest).listen(3000)

