
CREATE DATABASE IF NOT EXISTS my_test_db;
USE my_test_db;
CREATE TABLE IF NOT EXISTS `quotes` (

  `id` int(11) NOT NULL auto_increment,          
  `quote` varchar(250)  NOT NULL default '',     
  `author`  varchar(250) default '', 
   PRIMARY KEY  (`id`)

);
TRUNCATE TABLE quotes;
INSERT INTO `quotes` VALUES (1,'You know you’re in love when you can’t fall asleep because reality is finally better than your dreams','Dr Suess');
INSERT INTO `quotes` VALUES (2,'Great minds discuss ideas; average minds discuss events; small minds discuss people','Eleanor Roosevelt');
INSERT INTO `quotes` VALUES (3,'Those who dare to fail miserably can achieve greatly','JF Kennedy');
INSERT INTO `quotes` VALUES (4,'Let us always meet each other with smile, for the smile is the beginning of love','Mother Teresa');
INSERT INTO `quotes` VALUES (5,'Love is a serious mental disease','Plato');
INSERT INTO `quotes` VALUES (6,'It is our choices, that show what we truly are, far more than our abilities',' J. K Rowling');
INSERT INTO `quotes` VALUES (7,'Only put off until tomorrow what you are willing to die having left undone.','Pablo Picasso');
INSERT INTO `quotes` VALUES (8,'If you want to be happy, be','Leo Tolstoy');
INSERT INTO `quotes` VALUES (9,'If you want to live a happy life, tie it to a goal, not to people or things',' Albert Einstein');
INSERT INTO `quotes` VALUES (10,'Life is trying things to see if they work','Ray Bradbury');